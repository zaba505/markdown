module gitlab.com/zaba505/markdown

go 1.13

require (
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	gitlab.com/golang-commonmark/html v0.0.0-20180917080848-cfaf75183c4a
	gitlab.com/golang-commonmark/linkify v0.0.0-20191026162114-a0c2df6c8f82
	gitlab.com/golang-commonmark/mdurl v0.0.0-20180912090424-e5bce34c34f2
	gitlab.com/golang-commonmark/puny v0.0.0-20180912090636-2cd490539afe
	gitlab.com/opennota/wd v0.0.0-20180912061657-c5d65f63c638
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/russross/blackfriday.v2 v2.0.0
)
